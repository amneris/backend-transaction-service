# Backend Transaction Service

This microservice manages payment transactions.

**Table of Contents**
<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:0 orderedList:0 -->

- [Getting started](#getting-started)
	- [Database](#database)
	- [Message Broker](#message-broker)
- [How to use it](#how-to-use-it)

<!-- /TOC -->

## Getting started

This section is dedicated to show how to contribute to this project setting up the local environment.

## Prequisites

[Permit access to Nexus](https://github.com/abaenglish/software-guidebook/blob/master/tools/nexus.md)

### Database

This microservice needs a database connection and a schema called ```transaction-service```, you can use two methods.

* Installing [MySQL](https://dev.mysql.com/downloads/mysql/) and adding transaction-service schema.

or

* Running a docker image: ```docker run -p 3306:3306 -e MYSQL_ROOT_PASSWORD=admin -e MYSQL_DATABASE=transaction-service --name mysql mysql:5.7```

### Message Broker

Transaction service needs a message broker. We use RabbitMQ so you have two options.

* Installing [RabbitMQ](https://www.rabbitmq.com/download.html).

or

* Running a docker image: ```docker run -p 4369:4369 -p 5671-5672:5671-5672 -p 15671-15672:15671-15672 -p 25672:25672 --name rabbitmq rabbitmq:3-management```

## How to use it

This section defines how to execute transaction service to be used as a resource for other projects as if it was running on a server in http://localhost:8080.

You can execute it as a service using our docker image as follows: ```docker run -p 8080:8080 --link mysql:database --link rabbitmq:rabbitmq nexus.aba.land:5000/transaction-service:latest --spring.datasource.url="jdbc:mysql://database/transaction-service?useSSL=false" --spring.rabbitmq.host=rabbitmq```

This method will need a database and a message broker. You can use the docker images described in the [previous section](#getting-started).

